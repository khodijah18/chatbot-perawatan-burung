# Chatbot Perawatan Burung



## Getting started

Chatbot perawatan burung peliharaan merupakan sebuah aplikasi teknologi Natural Language Processing yang dibangun dengan menggunakan bahasa pemrograman python dan digunakan melalui aplikasi chatting telegram. Aplikasi chatbot ini terbatas pada perawatan burung peliharaan, dimana untuk saat ini hanya dapat menjawab pertanyaan seputar 4 jenis burung yaitu Kenari, Merpati, Lovebird, dan Parkit seputar cara perawatan, makanan yang dikonsumsi, penyakit yang kerap terjangkit, dan kebutuhan khusus dari tiap-tiap burung. Besar kemungkinan akan dilakukan pengembangan sehingga dapat memberikan manfaat yang lebih luas. Tata cara penggunaannya pun cukup mudah, pengguna bisa mengunjungi bot telegram kami (link) kemudian sudah dapat mengajukan pertanyaan. Pada saat ini, bot kami hanya menerima input berupa kalimat. Dan dikarenakan belum dilakukan deployment untuk chatbot ini, maka bot hanya dapat diakses ketika pengembang aplikasi menjalankan chatbot tersebut. Namun chatbot tetap dapat digunakan apabila dijalankan oleh pihak lain dengan bot telegram milik masing-masing.